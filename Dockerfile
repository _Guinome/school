FROM node:8
RUN mkdir /school
ADD . /school
WORKDIR /school
RUN npm i
EXPOSE 80
CMD ["npm", "start"]